import React from 'react';
import { Switch } from 'react-router-dom';
import Loadable from 'react-loadable';

import Login from 'src/app/modules/login/login';
import Register from 'src/app/modules/account/register/register';
import Activate from 'src/app/modules/account/activate/activate';
import PasswordResetInit from 'src/app/modules/account/password-reset/init/password-reset-init';
import PasswordResetFinish from 'src/app/modules/account/password-reset/finish/password-reset-finish';
import Logout from 'src/app/modules/login/logout';
import Home from 'src/app/modules/home/home';
import Entities from 'src/app/entities';
import PrivateRoute from 'src/app/shared/auth/private-route';
import ErrorBoundaryRoute from 'src/app/shared/error/error-boundary-route';
import PageNotFound from 'src/app/shared/error/page-not-found';
import { AUTHORITIES } from 'src/app/config/constants';

const Account = Loadable({
  loader: () => import(/* webpackChunkName: "account" */ 'src/app/modules/account'),
  loading: () => <div>loading ...</div>,
});

const Admin = Loadable({
  loader: () => import(/* webpackChunkName: "administration" */ 'src/app/modules/administration'),
  loading: () => <div>loading ...</div>,
});

const Routes = () => (
  <div className="view-routes">
    <Switch>
      <ErrorBoundaryRoute path="/login" component={Login} />
      <ErrorBoundaryRoute path="/logout" component={Logout} />
      <ErrorBoundaryRoute path="/account/register" component={Register} />
      <ErrorBoundaryRoute path="/account/activate/:key?" component={Activate} />
      <ErrorBoundaryRoute path="/account/reset/request" component={PasswordResetInit} />
      <ErrorBoundaryRoute path="/account/reset/finish/:key?" component={PasswordResetFinish} />
      <PrivateRoute path="/admin" component={Admin} hasAnyAuthorities={[AUTHORITIES.ADMIN]} />
      <PrivateRoute path="/account" component={Account} hasAnyAuthorities={[AUTHORITIES.ADMIN, AUTHORITIES.USER]} />
      <ErrorBoundaryRoute path="/" exact component={Home} />
      <PrivateRoute path="/" component={Entities} hasAnyAuthorities={[AUTHORITIES.USER]} />
      <ErrorBoundaryRoute component={PageNotFound} />
    </Switch>
  </div>
);

export default Routes;
